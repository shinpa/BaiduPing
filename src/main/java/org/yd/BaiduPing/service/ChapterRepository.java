package org.yd.BaiduPing.service;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.yd.BaiduPing.entity.TChatper;

public interface ChapterRepository extends CrudRepository<TChatper, Integer> {

    @Query("SELECT t from TChatper t WHERE chapterNo > :no and deleteflag = false order by chapterno ")
    List<TChatper> fetch(@Param("no") Integer chapterNo, Pageable pageable);

//    @Modifying
//    @Query("UPDATE TChatper t SET t.pingbaidu = true WHERE t.chapterNo = :no")
//    Integer updateChapterByNo(@Param("no") Integer chapterNo);
}
