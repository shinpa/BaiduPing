package org.yd.BaiduPing.service;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.yd.BaiduPing.entity.TArticle;

public interface ArticleRepository extends CrudRepository<TArticle, Integer> {

    @Query("SELECT t from TArticle t WHERE articleNo > :no and deleteflag = false order by articleno ")
    List<TArticle> fetch(@Param("no") Integer articleNo, Pageable pageable);

    TArticle findByArticleNo(Integer articleNo);

//    @Modifying
//    @Query("UPDATE TArticle t SET t.pingbaidu = true WHERE t.articleNo = :no")
//    Integer updateArticleByNo(@Param("no") Integer articleNo);
}
