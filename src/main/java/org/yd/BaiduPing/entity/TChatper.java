package org.yd.BaiduPing.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "t_chapter")
@Data
public class TChatper {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "t_chapter_chapterno_seq")
    @SequenceGenerator(name = "t_chapter_chapterno_seq", sequenceName = "t_chapter_chapterno_seq", allocationSize = 1)
    @Column(name = "chapterno")  
    private Integer chapterNo;
    @Column(name = "articleno")  
    private Integer articleNo;
//    private Short chaptertype;
//    private Boolean pingbaidu;
}
