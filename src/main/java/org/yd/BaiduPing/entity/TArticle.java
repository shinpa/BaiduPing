package org.yd.BaiduPing.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "t_article")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TArticle {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "t_article_articleno_seq")
    @SequenceGenerator(name = "t_article_articleno_seq", sequenceName = "t_article_articleno_seq", allocationSize = 1)
    @Column(name = "articleno")  
    private Integer articleNo;
    private String pinyin;
//    private Boolean pingbaidu;
}
